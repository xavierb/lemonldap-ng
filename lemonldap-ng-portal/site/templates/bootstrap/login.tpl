<TMPL_INCLUDE NAME="header.tpl">

<main id="logincontent" class="container">

  <TMPL_INCLUDE NAME="customLoginHeader.tpl">

  <div id="errormsg">
    <TMPL_IF NAME="AUTH_ERROR">
      <div class="message message-<TMPL_VAR NAME="AUTH_ERROR_TYPE"> alert"><span trmsg="<TMPL_VAR NAME="AUTH_ERROR">"></span>
        <TMPL_IF LOCKTIME>
          <TMPL_VAR NAME="LOCKTIME"> <span trspan="seconds">seconds</span>.
        </TMPL_IF>
      </div>
    </TMPL_IF>
  </div>

  <div class="card">
  <TMPL_IF NAME="module">
    <form id="lform" action="#" method="post" class="login <TMPL_VAR NAME="module">" role="form">
  <TMPL_ELSE>
    <form id="lform" action="#" method="post" class="login" role="form">
  </TMPL_IF>
    <TMPL_VAR NAME="HIDDEN_INPUTS">
    <input type="hidden" name="url" value="<TMPL_VAR NAME="AUTH_URL">" />
    <input type="hidden" name="timezone" />
    <input type="hidden" name="skin" value="<TMPL_VAR NAME="SKIN">" />
    __LLNG_LOGIN_FORM__
  </form>
  </div>
  
  <TMPL_INCLUDE NAME="finduser.tpl">
  <TMPL_INCLUDE NAME="customLoginFooter.tpl">

</main>

<TMPL_INCLUDE NAME="footer.tpl">
