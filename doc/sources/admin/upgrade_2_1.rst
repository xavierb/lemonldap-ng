Upgrade from 2.0 to 2.1
=======================

SOAP deprecation
----------------

LLNG 2.1.x will be the last major version supporting SOAP services.
Please start migration to :doc:`REST services<restservices>` *(available
since 2.0.0)*.
